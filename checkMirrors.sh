#!/bin/bash
source /home/ffa/drive/scriptFiles/globalSciptConfig.sh
echo "Checking these mirrors ${questMirrors[@]}..."
for mirror in ${questMirrors[@]}
do
	rclone cat FFA-"$mirror":/Quest\ Games/.meta/checkDown > /dev/null 2>&1
	if [ $? -eq 0 ]
	then
		echo "FFA-"$mirror" is up. Now checking files..."
		for file in $(rclone lsf "FFA-02:/Quest Games/"  --files-only)
		do
			rclone cat FFA-"$mirror":/Quest\ Games/"$file" > /dev/null 2>&1
			if [ $? -eq 0 ]
			then
				echo "	$file is up"
			else
				echo "	$file is down"
			fi
		done
	else
		echo "FFA-"$mirror" seems down (whole mirror)"
	fi
done
echo "Done checking"
