#!/bin/bash
for mirror in "$@"
do
        echo -n "FFA-$mirror started at $(date +'%R:%S-%d/%m/%Y')..."  >> /var/log/ffa/newsync.log
        rclone sync --bwlimit 35M --drive-chunk-size 128M -v --stats 6s --transfers 15 --checkers 8 --contimeout 60s --timeout 300s --retries 3 --low-level-retries 10 /home/ffa/drive/Quest\ Games/ FFA-"$mirror":/Quest\ Games/
        echo " done"  >> /var/log/ffa/newsync.log
done
