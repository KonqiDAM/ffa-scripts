#!/bin/bash
source /home/ffa/drive/scriptFiles/globalSciptConfig.sh
if [ "$stopCreateGameListBeta" = true ] ; then
    echo 'GameList creation is stopped!! exiting...'
	exit
fi
echo "Generaing new game list..."
echo "Game Name;Release Name;Package Name;Version Code;Last Updated;Size (MB)" > /home/ffa/drive/autoCracked/_done/FFA.txt
find "/home/ffa/drive/autoCracked/_done" -name "* *" -type d | rename 's/  */ /g'
find "/home/ffa/drive/autoCracked/_done" -maxdepth 1 -type d -printf '%P\n' > tempDone.txt
count=1
failed=false
while IFS= read -r line; do
        ((count--))
        if [ $count -ge 0 ]
        then
                continue;
        fi
        if [[ "$line" =~ ".txt" ]]; then
                continue;
        fi
	if [[ "$line" =~ ".meta" ]]; then
                continue;
        fi
        releaseName=$(echo "$line")
        exception=$(cat /home/ffa/drive/scriptFiles/exceptions.txt | grep "$releaseName")
	if [ -n "${exception}" ]; then
		echo "FOUND $exception in exceptions"
		size=$(du -ms "/home/ffa/drive/autoCracked/_done/$releaseName" | sed 's/[[:space:]]/ /g' | cut -d ' ' -f 1)
                lastUpdate=$(date -r "$apk" +"%Y-%m-%d %H:%M UTC")
		echo "$exception;$lastUpdate;$size" >> /home/ffa/drive/autoCracked/_done/FFA.txt
	else
	        path=$(echo "$releaseName" | sed 's/ /\\ /g' )
	        apk=$(ls "/home/ffa/drive/autoCracked/_done/$releaseName/"*apk)
	        packageName=$(aapt dump badging "$apk" | grep package:\ name | awk -F"'" '/package: name=/{print $2}')
		if [ -z "${packageName}" ]; then
			echo "Package empty for $releaseName, will try alternative aapt"
			packageName=$(aapt2 dump badging "$apk" | grep package:\ name | awk -F"'" '/package: name=/{print $2}')
	        fi
		if [ -z "${packageName}" ]; then
	                echo "Package empty, not adding line"
			failed=true
	        else
		        label=$(aapt dump badging "$apk" | grep application-label | awk -F"'" '/application-label:/{print $2}')
			gameName=$(echo $releaseName | awk -F 'v[0-9]' ' { print $1 }')
		        versionCode=$(aapt dump badging "$apk" | grep versionCode= | awk -F"'" '/versionCode=/{print $4}')
		        aplicationLabel=$(aapt dump badging "$apk" | grep application-label | awk -F"'" '/application-label:/{print $2}')
		        size=$(du -ms "/home/ffa/drive/autoCracked/_done/$releaseName" | sed 's/[[:space:]]/ /g' | cut -d ' ' -f 1)
		        lastUpdate=$(date -r "$apk" +"%Y-%m-%d %H:%M UTC")
		        echo "$gameName;$releaseName;$packageName;$versionCode;$lastUpdate;$size" >> /home/ffa/drive/autoCracked/_done/FFA.txt
		#        echo "$label;$releaseName;$packageName;$versionCode;$lastUpdate;$size"
		fi
	fi
done < tempDone.txt
rm /home/ffa/drive/autoCracked/_done/FFA2.txt /home/ffa/drive/autoCracked/_done/FFA3.txt /home/ffa/drive/autoCracked/_done/FFA4.txt
cp /home/ffa/drive/autoCracked/_done/FFA.txt /home/ffa/drive/autoCracked/_done/FFA2.txt
cp /home/ffa/drive/autoCracked/_done/FFA.txt /home/ffa/drive/autoCracked/_done/FFA3.txt
cp /home/ffa/drive/autoCracked/_done/FFA.txt /home/ffa/drive/autoCracked/_done/FFA4.txt
rm tempDone.txt
echo "game list done"
if [ "$failed" == true ]; then
	echo "Errors detected... sending message to group"
#	/home/ffa/telegram-bot-bash/bin/send_message.sh "-1001763061332" "It seems like there are errors one the game list generation, please fix it or I will remind you every time!!\nSync still in progress. Watch logs here $(cat /var/log/ffa/gameListDone.log | pastebinit)" &
fi
