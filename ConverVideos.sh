#!/bin/bash
##NEEDS TUNNING; SET CORRECT FOLDER BEFORE EXECUTION
for i in *.mp4;
do
	OUTPUT=$(mediainfo --Output='General;%Duration%' -i "$i")
	if [ $OUTPUT -gt 65000 ]; then 
		ffmpeg -y -i "$i" -to 00:01:05 -c copy "${i%.*}_1.mp4";
	else 
		cp "$i" "${i%.*}_1.mp4";
	fi

	ffmpeg -y -ss 00:00:05 -i "${i%.*}_1.mp4" -tune animation -c:v libx264 -preset veryslow -crf 22 -filter:v "scale=w=414:h=232,fps=fps=20" -an "${i%.*}_2.mp4"

	ffmpeg -y -i "${i%.*}_2.mp4" -filter_complex "[0:v]setpts=0.5*PTS[v]" -map "[v]" -an "${i%.*}_DONE.mp4"
	rm -rf "${i%.*}_1.mp4"
	rm -rf "${i%.*}_2.mp4"
	mv "$i" "./old"/"${i%.*}_OLD.mp4"
	mv "${i%.*}_DONE.mp4" "$i"
done
