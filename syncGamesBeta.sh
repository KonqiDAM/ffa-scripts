#!/bin/bash
source /home/ffa/drive/scriptFiles/globalSciptConfig.sh
if [ "$stopSyncGamesBeta" = true ] ; then
	echo 'Games syncing is stopped!! exiting...'
	echo "Games syncing is stopped!!" >> /var/log/ffa/syncBeta.log
	exit
fi
if { set -C; 2>/dev/null >~/manlockBeta.lock; }; then
	trap "rm -f ~/manlockBeta.lock" EXIT
else
	echo "Lock file exists… exiting"
	echo "Lock file exists… exiting" >> /var/log/ffa/syncBeta.log
	exit
fi
echo "--------------------------------------------" >> /var/log/ffa/syncBeta.log
echo "Starting new sync beta: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/syncBeta.log
echo "Generating game list Beta" >> /var/log/ffa/syncBeta.log
start=`date +%s.%N`
/home/ffa/ffascripts/createGameListBeta.sh >| /var/log/ffa/gameListBeta.log 2>&1
end=`date +%s.%N`
echo "list done. Used time $( echo "$end - $start" | bc -l )" >> /var/log/ffa/syncBeta.log
echo "Starting Quest sync: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/syncBeta.log
echo -n "FFA-Beta started at $(date +'%R:%S-%d/%m/%Y')..."  >> /var/log/ffa/syncBeta.log
rclone sync --bwlimit 40M --drive-chunk-size 128M --log-file /var/log/ffa/syncProgressBeta.log -v --stats 60s --transfers 15 --checkers 8 --contimeout 60s --timeout 300s --retries 3 --low-level-retries 10 /home/ffa/drive/QuestBeta FFA-Beta:/Quest\ Games/
echo " done"  >> /var/log/ffa/syncBeta.log
echo "All done: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/syncBeta.log
echo "Sync done"
