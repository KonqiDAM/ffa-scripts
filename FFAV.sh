#!/bin/bash
PATH=/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
mkdir /FFV/old 2>/dev/null
mkdir /FFV 2>/dev/null
GAMESLIST=$(cat /home/ffa/drive/Quest\ Games/FFA.txt)
existingTrailers=(/home/ffa/drive/Quest\ Games/.meta/videos/*)
while IFS= read -r line;
do
	if [ -z "$line" ]; then
		continue;
	fi
	game=$(echo "$line" | cut -d ';' -f3)
	#echo "$game"
	if [[ "$game" == "Package Name" ]]; then
		continue;
	fi
	#fileOut=$(echo "/home/ffa/drive/Quest\ Games/.meta/videos/$game.mp4" | tr '[:upper:]' '[:lower:]')
	#echo "searching $fileOut"
	#exists=$(printf '%s\0' "${existingTrailers[@]}" | tr '[:upper:]' '[:lower:]' | grep -F -x -z -- "$fileOut")
	#printf '%s\0' "${existingTrailers[@]}" | tr '[:upper:]' '[:lower:]' | grep -F -x -z -- "$fileOut"
	exists=$(echo "${existingTrailers[@]}" | grep -i "$game")
	#echo "EXISTS VALUE IS: $exists"
	if ! [ -z  "$exists" ]; then
#		echo "$FILE exists. skiping downlad of $game";
		continue;
	else
		echo "Didn't find file for $game. Starting download";
	fi
	json=$(curl -s "https://qloader.5698452.xyz/api/v1/oculusgames/$game" )
	trailerUrl=$(echo "$json" | jq .trailer_url | tr -d '"')
	if [ -z "$trailerUrl" ] || [ null == "$trailerUrl" ]; then
		echo "trailer ULR not found!!!!"
		continue;
	fi;
	echo "trying to download $trailerUrl"
	wget "$trailerUrl" -O "$game".mp4 --no-verbose -q
done <<< "$GAMESLIST"
