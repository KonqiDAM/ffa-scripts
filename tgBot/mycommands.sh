#!/bin/bash
#######################################################
#
#        File: mycommands.sh.clean
#
# copy to mycommands.sh and add all your commands and functions here ...
#
#       Usage: will be executed when a bot command is received 
#
#     License: WTFPLv2 http://www.wtfpl.net/txt/copying/
#      Author: KayM (gnadelwartz), kay@rrr.de
#
#### $$VERSION$$ v1.51-0-g6e66a28
#######################################################
# shellcheck disable=SC1117

####################
# Config has moved to bashbot.conf
# shellcheck source=./commands.sh
[ -r "${BASHBOT_ETC:-.}/mycommands.conf" ] && source "${BASHBOT_ETC:-.}/mycommands.conf"  "$1"


##################
# lets's go
if [ "$1" = "startbot" ];then
	###################
	# this section is processed on startup

	# run once after startup when the first message is received
	my_startup(){
	:
	}
	touch .mystartup
else
	# call my_startup on first message after startup
	# things to do only once
	[ -f .mystartup ] && rm -f .mystartup && _exec_if_function my_startup

	#############################
	# your own bashbot commands
	# NOTE: command can have @botname attached, you must add * to case tests...
	mycommands() {
		##############
		# a service Message was received
		# add your own stuff here
		if [ -n "${SERVICE}" ]; then
			# example: delete every service message
			if [ "${SILENCER}" = "yes" ]; then
				delete_message "${CHAT[ID]}" "${MESSAGE[ID]}"
			fi
		fi

		# remove keyboard if you use keyboards
		[ -n "${REMOVEKEYBOARD}" ] && remove_keyboard "${CHAT[ID]}" &
		[[ -n "${REMOVEKEYBOARD_PRIVATE}" &&  "${CHAT[ID]}" == "${USER[ID]}" ]] && remove_keyboard "${CHAT[ID]}" &

		# uncommet to fix first letter upper case because of smartphone auto correction
		#[[ "${MESSAGE}" =~  ^/[[:upper:]] ]] && MESSAGE="${MESSAGE:0:1}$(tr '[:upper:]' '[:lower:]' <<<"${MESSAGE:1:1}")${MESSAGE:2}"
		if user_is_botadmin "${USER[ID]}" || user_is_allowed "${USER[ID]}" "*" "${CHAT[ID]}"; then
			case "${MESSAGE}" in
				##################
				# example command, replace them by your own
				##########
				# command overwrite examples
				# return 0 -> run default command afterwards
				# return 1 -> skip possible default commands
				'/start'*) # disable all commands starting with leave
					return 1
					;;
				# replace command with your own actions
				'/kickme'*) # this will replace the /kickme command
					return 1
					;;
				# extend global command
				'/info'*) # output date in front of regular info
					return 1
					;;
				'/help'*)
					return 1
					;;
				'/leavechat'*)
					return 1
					;;
				'/gamelistlogs'*)
					send_html_message "${CHAT[ID]}" "<code>$(cat /var/log/ffa/gameList.log)</code>"
					;;
				'/synclogsbeta'*)
					send_html_message "${CHAT[ID]}" "<code>$(tail /var/log/ffa/syncBeta.log)</code>"
					savedid="${BOTSENT[ID]}"
					edit_inline_buttons "${CHAT[ID]}" "${savedid}" "Update|UPDATELOGBETA"
					;;
				'/synclogs'*)
					send_html_message "${CHAT[ID]}" "<code>$(tail /var/log/ffa/sync.log -n 43)</code>"
					sleep 1;
					savedid="${BOTSENT[ID]}"
					edit_inline_buttons "${CHAT[ID]}" "${savedid}" "Update|UPDATELOG"
					;;
				'/syncpcvrbeta'*)
					if [ -f /home/ffa/manlockPCVRBeta.lock ];
					then
						send_message "${CHAT[ID]}" "Already syncing, use /synclogsbeta. In case its not syncing ping Ivan"
					else
						send_html_message "${CHAT[ID]}" "Sync started, use /synclogsbeta after a few minutes to see progress."
						/home/ffa/ffascripts/syncGamesPCVRBeta.sh &
					fi
					;;
				'/forcesyncbeta'*)
					if [ -f /home/ffa/manlockBeta.lock ];
					then
						send_message "${CHAT[ID]}" "Already syncing, use /synclogsbeta. In case its not syncing ping Ivan"
					else
						send_html_message "${CHAT[ID]}" "Sync started, use /synclogsbeta after a few minutes to see progress."
						/home/ffa/ffascripts/syncGamesBeta.sh &
					fi
					;;
				'/forcesync'*)
					if [ -f /home/ffa/manlock.lock ];
					then
						send_message "${CHAT[ID]}" "Already syncing, use /synclogs. In case its not syncing ping Ivan"
					else
						send_html_message "${CHAT[ID]}" "Sync started, use /synclogs after a few minutes to see progress."
						/home/ffa/ffascripts/syncGames.sh &
					fi
					;;
				'/countgamelist'*)
					lines=$(cat /home/ffa/drive/Quest\ Games/FFA2.txt | wc -l)
					send_markdownv2_message "${CHAT[ID]}" "We got *$((lines-1))* games!!!"
					;;
				'/date'*)
					send_message "${CHAT[ID]}" "$(date +'%R:%S-%d/%m/%Y')"
					;;
				'/syncprogress'*)
					send_message "${CHAT[ID]}" "$(tail -n 300 /var/log/ffa/syncProgress.log | pastebinit)"
					;;
				'/crack'*)
					if { set -C; 2>/dev/null >/home/ffa/cracklock.lock; }; then
						trap "rm -f /home/ffa/cracklock.lock" EXIT
					else
						send_message "${CHAT[ID]}" "Already cracking, wait until it ends"
						echo "${USER[FIRST_NAME]} ${USER[LAST_NAME]} (${USER[USERNAME]}) used /crack in but it was locked ${CHAT[FIRST_NAME]} at ${CHAT[LAST_NAME]} at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
						return 1;
					fi
					if [ "$(ls /home/ffa/lockDownload)" ]; then
						send_message "${CHAT[ID]}" "There are downloads in progress, please wait and launch /crack again"
						echo "${USER[FIRST_NAME]} ${USER[LAST_NAME]} (${USER[USERNAME]}) used /crack in but downloads where in progress at ${CHAT[FIRST_NAME]} ${CHAT[LAST_NAME]} at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
					else
						echo "${USER[FIRST_NAME]} ${USER[LAST_NAME]} (${USER[USERNAME]}) used /crack and it started at ${CHAT[FIRST_NAME]} ${CHAT[LAST_NAME]} at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
						send_message "${CHAT[ID]}" "Cracking started... dont launch another one"
						rm /home/ffa/package.txt
						log=$(/usr/local/bin/runQC.sh | pastebinit)
						rm /home/ffa/package.txt
						send_message "${CHAT[ID]}" "Crack ended, here you have the logs: $log"
						rm -f /home/ffa/cracklock.lock
						/home/ffa/ffascripts/syncGamesBeta.sh &
					fi
					;;
				'/syncmeta'*)
					if [ -f /home/ffa/manlock.lock ];
					then
						echo "${USER[FIRST_NAME]} ${USER[LAST_NAME]} (${USER[USERNAME]}) used /aynmeta but was blocked at ${CHAT[FIRST_NAME]} ${CHAT[LAST_NAME]} at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
						send_message "${CHAT[ID]}" "Already syncing, use /synclogs. In case its not syncing ping Ivan"
					else
						echo "${USER[FIRST_NAME]} ${USER[LAST_NAME]} (${USER[USERNAME]}) used /syncmeta and it started at ${CHAT[FIRST_NAME]} ${CHAT[LAST_NAME]} at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
						send_html_message "${CHAT[ID]}" "Sync started, use /synclogs after a few minutes to see progress."
						/home/ffa/ffascripts/syncMeta.sh &
					fi
					;;
				'/detectnewgames'*)
					send_message "${CHAT[ID]}" "Launching script..."
					echo "${USER[FIRST_NAME]} ${USER[LAST_NAME]} (${USER[USERNAME]}) used /detecnewgames and it started at ${CHAT[FIRST_NAME]} ${CHAT[LAST_NAME]} at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
					/home/ffa/ffascripts/detectNewGames.sh &
					;;
				'/checkmirrors'*)
					logFile=$(date +'%Y-%m-%d-%R:%S')
					send_normal_message "${CHAT[ID]}" "Watch logs here https://ffa.404.mn:777/logs/mirrorStatus$logFile.txt"
					/home/ffa/ffascripts/checkMirrors.sh > "/home/ffa/web/logs/mirrorStatus$logFile.txt" 2>&1
					;;
				'/who'*)
					send_normal_message "${CHAT[ID]}" "Logs here:\n$(ps -ef | grep '[s]shd:.*@notty')"
					;;
				'/speedtest'*)
					send_normal_message "${CHAT[ID]}" "$(speedtest | grep 'Result URL:'). Speed can be slower than real if a sync is in progress"
					;;
			esac
		else
			send_normal_message "9363112" "uso no autorizado por ${USER[FIRST_NAME]} ${USER[LAST_NAME]} (@${USER[USERNAME]}) en el chat ${CHAT[FIRST_NAME]} ${CHAT[LAST_NAME]}"
			return 1;
		fi
	}

	mycallbacks() {
		#######################
		# callbacks from buttons attached to messages will be  processed here
		case "${iBUTTON[USER_ID]}+${iBUTTON[CHAT_ID]}" in
			*)	# all other callbacks are processed here
			local callback_answer
			#callback_answer="Working on it!"
			callback_answer="${iBUTTON[DATA]}"
			if [[ "${iBUTTON[DATA]}" =~ "C_" ]]; then
				package=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:2}" | cut -f2 -d';')
				origin=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:2}" | cut -f3 -d';')
				echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used button Crack for $package from $origin at NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
				if [ -z "${iBUTTON[DATA]:2}" ]; then
					callback_answer "No ID found, Ping Ivan and dont press any button"
					return;
				fi
				if { set -C; 2>/dev/null >/home/ffa/cracklock.lock; }; then
					trap "rm -f /home/ffa/cracklock.lock" EXIT
				else
					callback_answer "Crackin in progress, use it later again pls"
					return 1;
				fi
				downGame "$package" "${iBUTTON[CHAT_ID]}" "true" "$origin" "${iBUTTON[MESSAGE_ID]}" "${iBUTTON[DATA]:2}"&
				rm -f /home/ffa/cracklock.lock
			fi
			if [[ "${iBUTTON[DATA]}" =~ "Move_" ]]; then
				echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used Move for ${iBUTTON[DATA]} at NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
				if [ -z "${iBUTTON[DATA]:5}" ]; then
					callback_answer "No ID found, Ping Ivan and dont press any button"
					return;
				fi
				moveToQuest "${iBUTTON[DATA]:5}"
				if [ "$?" == "0" ]; then
					callback_answer="Done"
				elif [ "$?" == "1" ]; then
					callback_answer="Error, file not found, try after game list is created or ping Ivan"
				elif [ "$?" == "2" ]; then
					callback_answer="More than 1 game to move. You have to delete old from _done!!"
				elif [ "$?" == "3" ]; then
					callback_answer="No game found in gamestatus.txt!!"
				fi
			fi
			if [[ "${iBUTTON[DATA]}" =~ "MoveFB_" ]]; then
				echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used Move from beta for ${iBUTTON[DATA]} at NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
				if [ -z "${iBUTTON[DATA]:7}" ]; then
					callback_answer "No ID found, Ping Ivan and dont press any button"
					return;
				fi
				moveToQuestFromBeta "${iBUTTON[DATA]:7}"
				if [ "$?" == "0" ]; then
					callback_answer="Done"
				elif [ "$?" == "1" ]; then
					callback_answer="Error, file not found, try after game list is created or ping Ivan"
				elif [ "$?" == "2" ]; then
					callback_answer="More than 1 game to move. You have to delete old from beta!!"
				elif [ "$?" == "3" ]; then
					callback_answer="No game found in gamestatus.txt!!"
				fi
			fi
			if [[ "${iBUTTON[DATA]}" =~ "BetaM_" ]]; then
				echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used Move to beta for ${iBUTTON[DATA]} at NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
				if [ -z "${iBUTTON[DATA]:6}" ]; then
					callback_answer "No ID found, Ping Ivan and dont press any button"
					return;
				fi
				moveToBeta "${iBUTTON[DATA]:6}" "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}"
				if [ "$?" == "0" ]; then
					callback_answer="Done"
				elif [ "$?" == "1" ]; then
					callback_answer="Error, file not found, try after game list is created or ping Ivan"
				elif [ "$?" == "2" ]; then
					callback_answer="More than 1 game to move. You have to delete old from _done"
				elif [ "$?" == "3" ]; then
					callback_answer="No game found in gamestatus.txt!!"
				fi
			fi
			if [[ "${iBUTTON[DATA]}" =~ "BL_" ]]; then
				package=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:3}" | cut -f2 -d';')
				origin=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:3}" | cut -f3 -d';')
				echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used Blacklist for $package in $origin at NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
				callback_answer="$package was blacklisted"
				delete_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}"
				send_inline_buttons "-1001558757649" "$package from $origin was blacklisted by ${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]})" "Undo|Undo_${iBUTTON[DATA]:3}"
				blackList "$package" "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}"
			fi
			if [[ "${iBUTTON[DATA]}" =~ "GameDone_" ]]; then
				if [ -z "${iBUTTON[DATA]:9}" ]; then
					callback_answer "No ID found, Ping Ivan and dont press any button"
					return;
				fi
				package=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:9}" | cut -f2 -d';')
				origin=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:9}" | cut -f3 -d';')
				if [ $package -z ]; then
					callback_answer "No Package found, Ping Ivan and dont press any button"
				else
					echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used Done for $package from $origin in channel NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
					callback_answer="$package was marked as done"
					edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$package was marked as Done by ${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]})"
				fi
			fi
			if [[ "${iBUTTON[DATA]}" = "UPDATELOG" ]]; then
				edit_html_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "<code>$(tail  /var/log/ffa/sync.log -n 43)</code>"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Update|UPDATELOG"
			fi
			if [[ "${iBUTTON[DATA]}" = "UPDATELOGBETA" ]]; then
				edit_html_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "<code>$(tail /var/log/ffa/syncBeta.log)</code>"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Update|UPDATELOGBETA"
			fi
			if [[ "${iBUTTON[DATA]}" =~ "Undo_" ]]; then
				package=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:5}" | cut -f2 -d';')
				origin=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:5}" | cut -f3 -d';')
				game=$(echo "$package" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
				size=$(rclone size "FFA-DD:/_donations/$line")
				sed -i /$game/d /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt
				echo "${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]}) used Undo for $package from $origin in channel NEW PACKAGES at $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
				edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "NEW in $origin: $package.$(searchPlayStore $game)\n$size" 
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Crack it|C_${iBUTTON[DATA]:5}" "Done|GameDone_${iBUTTON[DATA]:5}" "Blacklist it|BL_${iBUTTON[DATA]:5}" "" "Google|https://www.google.com/search?q=$google VR Quest" "Applab|http://oculusapplab.com/?query=$google"
				callback_answer="$game was unblackslisted"
			fi
			if [[ "${iBUTTON[DATA]}" = "WORKSF" ]]; then
				message="${iBUTTON[MESSAGE]}"
				l1=$(echo "$message" | tail -5 | head -1 | cut -f2 -d':')
				suma=$(("$l1" + 1))
				result="$(echo "$message" | tail -6 | head -1 )\n$(echo "$message" | tail -5 | head -1 | cut -f1 -d':' ): "$suma" \n$(echo "$message" | tail -4 | head -1 )\n$(echo "$message" | tail -3 | head -1 )\n$(echo "$message" | tail -2 | head -1 )\n$(echo "$message" | tail -1 | head -1 )"
				edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$result"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Works fine|WORKSF" "Not working|WORKSN" "" "Muliplayer working|MULTIW" "Multiplayer not working|MULTIN" "Muliplayer untested|MULTIU"
			fi
			if [[ "${iBUTTON[DATA]}" = "WORKSN" ]]; then
				message="${iBUTTON[MESSAGE]}"
				l1=$(echo "$message" | tail -4 | head -1 | cut -f2 -d':')
				suma=$(("$l1" + 1))
				result="$(echo "$message" | tail -6 | head -1 )\n$(echo "$message" | tail -5 | head -1  )\n$(echo "$message" | tail -4 | head -1 | cut -f1 -d':'): $suma\n$(echo "$message" | tail -3 | head -1 )\n$(echo "$message" | tail -2 | head -1 )\n$(echo "$message" | tail -1 | head -1 )"
				edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$result"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Works fine|WORKSF" "Not working|WORKSN" "" "Muliplayer working|MULTIW" "Multiplayer not working|MULTIN" "Muliplayer untested|MULTIU"
			fi
			if [[ "${iBUTTON[DATA]}" = "MULTIW" ]]; then
				message="${iBUTTON[MESSAGE]}"
				l1=$(echo "$message" | tail -3 | head -1 | cut -f2 -d':')
				suma=$(("$l1" + 1))
				result="$(echo "$message" | tail -6 | head -1 )\n$(echo "$message" | tail -5 | head -1  )\n$(echo "$message" | tail -4 | head -1 )\n$(echo "$message" | tail -3 | head -1 | cut -f1 -d':'): $suma\n$(echo "$message" | tail -2 | head -1 )\n$(echo "$message" | tail -1 | head -1 )"
				edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$result"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Works fine|WORKSF" "Not working|WORKSN" "" "Muliplayer working|MULTIW" "Multiplayer not working|MULTIN" "Muliplayer untested|MULTIU"
			fi
			if [[ "${iBUTTON[DATA]}" = "MULTIN" ]]; then
				message="${iBUTTON[MESSAGE]}"
				l1=$(echo "$message" | tail -2 | head -1 | cut -f2 -d':')
				suma=$(("$l1" + 1))
				result="$(echo "$message" | tail -6 | head -1 )\n$(echo "$message" | tail -5 | head -1  )\n$(echo "$message" | tail -4 | head -1 )\n$(echo "$message" | tail -3 | head -1 )\n$(echo "$message" | tail -2 | head -1 | cut -f1 -d':'): $suma\n$(echo "$message" | tail -1 | head -1 )"
				edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$result"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Works fine|WORKSF" "Not working|WORKSN" "" "Muliplayer working|MULTIW" "Multiplayer not working|MULTIN" "Muliplayer untested|MULTIU"
			fi
			if [[ "${iBUTTON[DATA]}" = "MULTIU" ]]; then
				message="${iBUTTON[MESSAGE]}"
				l1=$(echo "$message" | tail -1 | head -1 | cut -f2 -d':')
				suma=$(("$l1" + 1))
				result="$(echo "$message" | tail -6 | head -1 )\n$(echo "$message" | tail -5 | head -1  )\n$(echo "$message" | tail -4 | head -1 )\n$(echo "$message" | tail -3 | head -1 )\n$(echo "$message" | tail -2 | head -1 )\n$(echo "$message" | tail -1 | head -1 | cut -f1 -d':'): $suma"
				edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$result"
				edit_inline_buttons "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "Works fine|WORKSF" "Not working|WORKSN" "" "Muliplayer working|MULTIW" "Multiplayer not working|MULTIN" "Muliplayer untested|MULTIU"
			fi
			# Telegram needs an ack each callback query, default empty
			answer_callback_query "${iBUTTON[ID]}" "${callback_answer}"
			;;
		esac
	}
	myinlines() {
		#######################
		# this fuinction is called only if you has set INLINE=1 !!
		# shellcheck disable=SC2128
		iQUERY="${iQUERY,,}"
		case "${iQUERY}" in
			##################
			# example inline command, replace it by your own
			"image "*) # search images with yahoo
				local search="${iQUERY#* }"
				answer_inline_multi "${iQUERY[ID]}" "$(my_image_search "${search}")"
				;;
		esac
	}

	searchPlayStore() {
		http_code=$(curl -LI https://play.google.com/store/apps/details?id=$1 -o /dev/null -w '%{http_code}\n' -s)
		if [ ${http_code} -eq 200 ]; then
			echo "\n!!!Package exist on playstore!!!"
		fi
	}

	blackList() {
		package=$(echo "$1" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
		echo "$package" >> /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt
	}

	downGame() {
		logFile=$(date +'%Y-%m-%d-%R:%S')
		actualFile="$1"
		edit_normal_message "$2" "$5" "[Downloading...] package in "$4": $actualFile\nLog: https://ffa.404.mn:777/logs/download$logFile.txt"
		find "/home/ffa/drive/autoCracked/_next" -size  0 -print -delete
		if [ -f "/home/ffa/lockDownload/$actualFile" ]; then
			send_message "$2" "Already downloading $actualFile. Not starting again"
		else
			touch "/home/ffa/lockDownload/$actualFile"
			echo "Download of $actualFile started $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
			if [ "$4" == "FFA" ]; then
				echo "Download command: rclone copy --log-file \"/home/ffa/web/logs/download$logFile.txt\" -v --stats 5s --bwlimit 30M \"FFA-DD:/_donations/$actualFile\"" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "FFA-DD:/_donations/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
			elif [ "$4" == "QU" ]; then
				echo "Download command: rclone copy --log-file \"/home/ffa/web/logs/download$logFile.txt\" -v --stats 5s --bwlimit 30M \"Garr-Glohnma:/clean/$actualFile\"" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "Garr-Glohnma:/clean/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				if [ -f "/home/ffa/drive/autoCracked/_next/$actualFile" ] && [ -s "/home/ffa/drive/autoCracked/_next/$actualFile" ] ; then
					echo "Download command: rclone copy --log-file \"/home/ffa/web/logs/download$logFile.txt\" -v --stats 5s --bwlimit 30M \"Garr-Glohnma:/applab/clean/$actualFile\"" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
					rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "Garr-Glohnma:/applab/clean/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				fi
			else
				echo "Download command: rclone copy --log-file \"/home/ffa/web/logs/download$logFile.txt\" -v --stats 5s --bwlimit 30M \"RSL-gameuploads:/$actualFile\"" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "RSL-gameuploads:/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				if [ -f "/home/ffa/drive/autoCracked/_next/$actualFile" ] && [ -s "/home/ffa/drive/autoCracked/_next/$actualFile" ] ; then
					rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "VRP_DONATION_TOKEN_1:/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				fi
				if [ -f "/home/ffa/drive/autoCracked/_next/$actualFile" ] && [ -s "/home/ffa/drive/autoCracked/_next/$actualFile" ] ; then
					rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "VRP_DONATION_TOKEN_2:/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				fi
				if [ -f "/home/ffa/drive/autoCracked/_next/$actualFile" ] && [ -s "/home/ffa/drive/autoCracked/_next/$actualFile" ] ; then
					rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 30M "RSL-gameuploads2:/$actualFile" /home/ffa/drive/autoCracked/_next >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				fi
			fi
			rm "/home/ffa/lockDownload/$actualFile"
			find "/home/ffa/drive/autoCracked/_next" -size  0 -print -delete
			if [ -f "/home/ffa/drive/autoCracked/_next/$actualFile" ] && [ -s "/home/ffa/drive/autoCracked/_next/$actualFile" ] ; then
				if [ "$3" == "true" ]; then
					echo "Download done, now cracking... $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
					edit_normal_message "$2" "$5" "[Cracking...] package in $4: $actualFile\nLog: https://ffa.404.mn:777/logs/download$logFile.txt"
					crack "$actualFile" "$2" "$5" "$4" "$logFile" "$6"
				else
					send_message "$2" "Download of $actualFile done. use you can download more games or use /crack"
				fi
			else
				echo "Download failed $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
				edit_normal_message "$2" "$5" "[Failed to download] package in $4: $actualFile\nLog: https://ffa.404.mn:777/logs/download$logFile.txt"
				edit_inline_buttons "$2" "$5" "Crack it!|C_$6" "Done|GameDone_$6"
			fi
		fi
	}

	crack() {
		/usr/local/bin/runQC.sh >> "/home/ffa/web/logs/download$5.txt" 2>&1
		echo "Crack done, now syncing $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/download$5.txt" 2>&1
		#edit_normal_message "$2" "$3" "[Syncing beta mirror...] package in $4: $1\nLog: https://ffa.404.mn:777/logs/download$5.txt"
		#/home/ffa/ffascripts/syncGamesBeta.sh >> "/home/ffa/web/logs/download$5.txt" 2>&1
		#echo "sync done $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/download$5.txt" 2>&1
		/home/ffa/ffascripts/createGameListDone.sh >| /var/log/ffa/gameListDone.log 2>&1
		edit_normal_message "$2" "$3" "[Cracked] package in $4: $1\nLog: https://ffa.404.mn:777/logs/download$5.txt"
		edit_inline_buttons "$2" "$3" "Done|GameDone_$6" "" "Move to Quest Games|Move_$6" "Move to Beta|BetaM_$6"
	}
	#####################
	# place your processing functions here
	# example inline processing function, not really useful
	# $1 search parameter
	my_image_search(){
		local image result sep="" count="1"
		result="$(wget --user-agent 'Mozilla/5.0' -qO - "https://images.search.yahoo.com/search/images?p=$1" |  sed 's/</\n</g' | grep "<img src=")"
		while read -r image; do
			[ "${count}" -gt "20" ] && break
			image="${image#* src=\'}"; image="${image%%&pid=*}"
			[[ "${image}" = *"src="* ]] && continue
			printf "%s\n" "${sep}"; inline_query_compose "${RANDOM}" "photo" "${image}"; sep=","
			count=$(( count + 1 ))
		done <<<"${result}"
	}
	
	moveToQuest() {
		filedrive=$(cat /home/ffa/gameStatus.txt | grep -F "$1" | cut -f2 -d';')
		if [ $filedrive -z ]; then return 3; fi
		package=$(echo "$filedrive" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
		fileNew=$(cat /home/ffa/drive/autoCracked/_done/FFA.txt | grep -F "$package" | cut -f2 -d';')
		versionCode=$(cat /home/ffa/drive/autoCracked/_done/FFA.txt | grep -F "$package" | cut -f4 -d';')
		gameName=$(echo $fileNew | cut -d'+' -f1 | rev | cut -d' ' -f2- | rev)
		version=$(echo $fileNew | cut -d'+' -f2 | cut -d' ' -f1)
		origin=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:9}" | cut -f3 -d';')
		needsVersionCode=$(cat /home/ffa/needsVersionCode.txt | $package )
		if [ ! -z "$needsVersionCode" ]; then
			version=versionCode
		fi
		if [ -z "$fileNew" ]
		then
			return 1;
		fi
		if [ "$(echo $fileNew | wc -l)" -qt "1"]
		then
			return 2;
		fi
		fileOld=$(cat /home/ffa/drive/Quest\ Games/FFA.txt | grep -F "$package" | cut -f2 -d';')
		count=$(cat /home/ffa/drive/Quest\ Games/FFA.txt | grep -F "$package" | cut -f2 -d';' | wc -l)
		text="$fileOld was moved to Previous Quest"
		if [ -z "$fileOld" ]; then
			text="No game was moved to Previous Quest"
			send_html_message "-1001682198216" "<b>$gameName v$version ($versionCode) added to Quest Games</b>"
		elif [ "$count" -gt 1 ]; then
			text="Found more than one game, not moving any game to Previous Quest"
			send_html_message "-1001682198216" "<b>$gameName updated to v$version ($versionCode) in Quest Games</b>"
		else
			send_html_message "-1001682198216" "<b>$gameName updated to v$version ($versionCode) in Quest Games</b>"
			mv "/home/ffa/drive/Quest Games/$fileOld" "/home/ffa/drive/Previous Quest/$fileOld"
		fi
		mv "/home/ffa/drive/autoCracked/_done/$fileNew" "/home/ffa/drive/Quest Games/$fileNew"
		edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$fileNew from $origin was moved to Quest Games. $text. Done by ${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]})"
		echo "Moving done $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt" 2>&1
		#/home/ffa/ffascripts/syncGamesBeta.sh
		#echo "sync done $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt" 2>&1
		return 0;
	}
	
	moveToBeta() {
		filedrive=$(cat /home/ffa/gameStatus.txt | grep -F "$1" | cut -f2 -d';')
		if [ $filedrive -z ]; then return 3; fi
		package=$(echo "$filedrive" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
		fileNew=$(cat /home/ffa/drive/autoCracked/_done/FFA.txt | grep -F "$package" | cut -f2 -d';')
		if [ -z "$fileNew" ]
		then
			return 1;
		fi
		if [ "$(echo $fileNew | wc -l)" -qt "1"]
		then
			return 2;
		fi
		mv "/home/ffa/drive/autoCracked/_done/$fileNew" "/home/ffa/drive/QuestBeta/$fileNew"
		edit_normal_message "$2" "$3" "$fileNew is moving to Quest Beta... Done by ${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]})"
		/home/ffa/ffascripts/syncGamesBeta.sh
		edit_normal_message "$2" "$3" "$fileNew was moved to Quest Beta. Done by ${iBUTTON[FIRST_NAME]} ${iBUTTON[LAST_NAME]} (${iBUTTON[USERNAME]})"
		edit_inline_buttons "$2" "$3" "Move to Quest Games from beta|MoveFB_$1" "" "Done|GameDone_$1"
		send_inline_buttons "-1001709171786" "New game to test: $fileNew\nWorks: 0\nNot working: 0\nMultiplayer works: 0\nMultiplayer not working: 0\nMultiplayer untested: 0" "Works fine|WORKSF" "Not working|WORKSN" "" "Muliplayer working|MULTIW" "Multiplayer not working|MULTIN" "Muliplayer untested|MULTIU"
		return 0;
	}
	
	moveToQuestFromBeta() {
		filedrive=$(cat /home/ffa/gameStatus.txt | grep -F "$1" | cut -f2 -d';')
		package=$(echo "$filedrive" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
		fileNew=$(cat /home/ffa/drive/QuestBeta/FFA.txt | grep -F "$package" | cut -f2 -d';')
		versionCode=$(cat /home/ffa/drive/QuestBeta/FFA.txt | grep -F "$package" | cut -f4 -d';')
		gameName=$(echo $fileNew | cut -d'+' -f1 | rev | cut -d' ' -f2- | rev)
		version=$(echo $fileNew | cut -d'+' -f2 | cut -d' ' -f1)
		origin=$(cat /home/ffa/gameStatus.txt | grep -F "${iBUTTON[DATA]:9}" | cut -f3 -d';')
		needsVersionCode=$(cat /home/ffa/needsVersionCode.txt | $package )
		if [ ! -z "$needsVersionCode" ]; then
			version=versionCode
		fi
		if [ -z "$fileNew" ]
		then
			return 1;
		fi
		if [ "$(echo $fileNew | wc -l)" -qt "1"]
		then
			return 2;
		fi
		fileOld=$(cat /home/ffa/drive/Quest\ Games/FFA.txt | grep -F "$package" | cut -f2 -d';')
		count=$(cat /home/ffa/drive/Quest\ Games/FFA.txt | grep -F "$package" | cut -f2 -d';' | wc -l)
		text="$fileOld was moved to Previous Quest"
		if [ -z "$fileOld" ]; then
			text="No game was moved to Previous Quest"
			send_html_message "-1001682198216" "<b>$gameName v$version ($versionCode) added to Quest Games</b>"
		elif [ "$count" -gt 1 ]; then
			text="Found more than one game, not moving any game to Previous Quest"
			send_html_message "-1001682198216" "<b>$gameName updated to v$version ($versionCode) in Quest Games</b>"
		else
			send_html_message "-1001682198216" "<b>$gameName updated to v$version ($versionCode) in Quest Games</b>"
			mv "/home/ffa/drive/QuestBeta/$fileOld" "/home/ffa/drive/Previous Quest/$fileOld"
		fi
		mv "/home/ffa/drive/QuestBeta/$fileNew" "/home/ffa/drive/Quest Games/$fileNew"
		edit_normal_message "${iBUTTON[CHAT_ID]}" "${iBUTTON[MESSAGE_ID]}" "$fileNew from $origin was moved to Quest Games."
		echo "Moving done. Star beta mirror sync $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt" 2>&1
		/home/ffa/ffascripts/syncGamesBeta.sh
		echo "sync done $(date +'%Y-%m-%d-%R:%S')" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt" 2>&1
		return 0;
	}

	###########################
	# example error processing
	# called when delete Message failed
	# func="$1" err="$2" chat="$3" user="$4" emsg="$5" remaining args
	bashbotError_delete_message() {
		log_debug "custom errorProcessing delete_message: ERR=$2 CHAT=$3 MSGID=$6 ERTXT=$5"
	}

	# called when error 403 is returned (and no func processing)
	# func="$1" err="$2" chat="$3" user="$4" emsg="$5" remaining args
	bashbotError_403() {
		log_debug "custom errorProcessing error 403: FUNC=$1 CHAT=$3 USER=${4:-no-user} MSGID=$6 ERTXT=$5"
	}
fi
