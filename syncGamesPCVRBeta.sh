#!/bin/bash
if { set -C; 2>/dev/null >~/manlockPCVRBeta.lock; }; then
        trap "rm -f ~/manlockPCVRBeta.lock" EXIT
else
        echo "Lock file exists… exiting"
        echo "Lock file exists… exiting" >> /var/log/ffa/syncBeta.log
        exit
fi
echo "--------------------------------------------" >> /var/log/ffa/sync.log
echo "Starting new syncBeta: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/syncBeta.log
echo "Generating game list" >> /var/log/ffa/syncBeta.log
pcvrMirrors=("Beta")
for mirror in ${pcvrMirrors[@]}
do
        echo -n "FFA-$mirror started at $(date +'%R:%S-%d/%m/%Y')..."  >> /var/log/ffa/syncBeta.log
        rclone sync --drive-chunk-size 128M --log-file /var/log/ffa/syncProgress.log -v --stats 60s --transfers 15 --checkers 8 --contimeout 60s --timeout 300s --retries 3 --low-level-retries 10 /home/ffa/drive/PCVRBeta/ FFA-"$mirror":/PCVRBeta
        echo " done"  >> /var/log/ffa/syncBeta.log
done
echo "All done: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/syncBeta.log
