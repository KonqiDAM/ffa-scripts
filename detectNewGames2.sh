#!/bin/bash
checkDownload() {
	game=$(echo "$1" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
	version=$(echo "$1" | rev | cut -d ' ' -f2 | rev | sed 's/v//g')
	#md5sum=$(echo -n "$1" "$2" | md5sum | awk '{printf $1}')
	find "/home/ffa/drive/autoCracked/_next" -size  0 -print -delete
	if { set -C; 2>/dev/null >/home/ffa/cracklock.lock; }; then
		trap "rm -f /home/ffa/cracklock.lock" EXIT
	else
		edit_normal_message "-1001739439303" "$3" "[Cracking already in progress. Try again later] new version in $2: $1\nLog: https://ffa.404.mn:777/logs/download$logFile.txt"
		edit_inline_buttons "-1001739439303" "$3" "Done|GameDone_$5" "Crack it|C_$5"
		return 1;
	fi
	echo "directory result:" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	echo "$(ls /home/ffa/drive/autoCracked/_next/)" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	echo "dolar 1 is: $1" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	echo "dolar 3 is: $3" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	if [ "$(ls -A /home/ffa/drive/autoCracked/_next/)" ]; then
		echo "Download of $1 is correct, now cracking..." >> "/home/ffa/web/logs/download$logFile.txt"
		/usr/local/bin/runQC.sh >> "/home/ffa/web/logs/download$logFile.txt" 2>&1
		/home/ffa/ffascripts/createGameListDone.sh
		gameName=$(cat /home/ffa/drive/autoCracked/_done/FFA2.txt | grep -F "$game" | cut -d';' -f2)
		echo "doing $gameName"
		edit_normal_message "-1001739439303" "$3" "$4\n[Auto cracked] new version in $2: $gameName.\nLog: https://ffa.404.mn:777/logs/download$logFile.txt"
		edit_inline_buttons "-1001739439303" "$3" "Done|GameDone_$5" "" "Move to Quest Games|Move_$5" "Move to Beta|BetaM_$5"
	else
		edit_normal_message "-1001739439303" "$3" "$4\n[Failed to download] new version in $2: $1\nLog: https://ffa.404.mn:777/logs/download$logFile.txt"
		edit_inline_buttons "-1001739439303" "$3" "Done|GameDone_$5" "Crack it|C_$5"
	fi
	rm -f /home/ffa/cracklock.lock
	#send_message "-1001739439303" "Error code: ${BOTSENT[ERROR]}\nError desc: ${BOTSENT[DESC]}"
}

searchPlayStore() {
	http_code=$(curl -LI https://play.google.com/store/apps/details?id=$1 -o /dev/null -w '%{http_code}\n' -s)
	if [ ${http_code} -eq 200 ]; then
		echo "\n!!!Package exist on playstore!!!";
	fi
}
logFile=$(date +'%Y-%m-%d-%R:%S')
source /home/ffa/drive/scriptFiles/globalSciptConfig.sh
if [ "$stopSearchingNewGames" = true ] ; then
	echo 'Gamesearch is stopped!! exiting...'
	echo "Gamesearch is stopped!! $logFile" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	exit
fi
if { set -C; 2>/dev/null >~/detectNewGames.lock; }; then
	trap "rm -f ~/detectNewGames.lock" EXIT
else
	echo "locked!!!!"
	exit
fi
export BASHBOT_HOME=/home/ffa/telegram-bot-bash
source ${BASHBOT_HOME}/bashbot.sh source
ffa=$(rclone lsf FFA-DD:/_donations --files-only --max-age 1d | grep -i -F -v -f /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt | grep -i -F -v -f /home/ffa/listedGames.txt)
vrp=$(rclone lsf RSL-gameuploads2:/ --files-only --max-age 1d | grep -i -F -v -f /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt | grep -i -F -v -f /home/ffa/listedGames.txt)
#qu=$(rclone lsf Garr-Glohnma:/clean --files-only --max-age 1d | grep -F -v -f /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt | grep -i -F -v -f /home/ffa/listedGames.txt)
#qu2=$(rclone lsf Garr-Glohnma:/applab/clean --files-only --max-age 1d | grep -F -v -f /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt | grep -i -F -v -f /home/ffa/listedGames.txt)
qu=""
qu2=""
echo "$ffa" >> /home/ffa/listedGames.txt
echo "$vrp" >> /home/ffa/listedGames.txt
echo "$qu" >> /home/ffa/listedGames.txt
echo "$qu2" >> /home/ffa/listedGames.txt
sort /home/ffa/listedGames.txt -f -b -u | grep "\S" > tempLines.txt
rm /home/ffa/listedGames.txt
mv tempLines.txt /home/ffa/listedGames.txt
echo "FFA: $ffa"
while IFS= read -r line;
do
	logFile=$(date +'%Y-%m-%d-%R:%S')
	echo "$line"
	if [ -z "$line" ]; then
		continue
	fi
	game=$(echo "$line" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
	version=$(echo "$line" | rev | cut -d ' ' -f2 | rev | sed 's/v//g')
	google=$(echo "$line" | rev | cut -d ' ' -f3- | rev )
	exist=$(grep -i -F "$game" /home/ffa/drive/Quest\ Games/FFA2.txt)
	md5sum=$(echo -n "$line""FFA" | md5sum | awk '{printf $1}')
	md5sumCheck=$(rclone cat "FFA-DD:/md5sum/$line.md5sum" | cut -c -32)
	md5result=""
	if [ -z "$md5sumCheck" ]; then
		md5result="md5sum file not found!!"
	else
		md5sumFile=$(rclone md5sum "FFA-DD:/_donations/$line" | cut -c -32)
		if [ "$md5sumCheck" == "$md5sumFile" ]; then
			md5result="md5sum is correct"
		else
			md5result="md5sum failed !!!!!!!!"
		fi
	fi
	echo "cheking $game"
	if [ -z "$exist" ]; then
		echo "$md5sum;$line;FFA" >> /home/ffa/gameStatus.txt
		size=$(rclone size "FFA-DD:/_donations/$line")
		storeResult=$(searchPlayStore "$game")
		oculusDBresult=$(curl https://oculusdb.rui2015.me/api/v1/packagename/$game)
		price=$(echo $oculusDBresult | jq .current_offer.price.formatted)
		idoculus=$(echo $oculusDBresult | jq .id )
		idoculusFinal=${idoculus//[\"]/}
		website=$(echo $oculusDBresult | jq .website_url)
		oculusRes=""
		#if [ -n "$idoculus" ]; then;
			oculusRes="\nStore: oculus.com/experiences/quest/$idoculusFinal\nPrice in store is $price\nWebsite is $website"
		#fi
		send_inline_buttons "-1001739439303" "$md5result\nNEW in FFA: $line.$storeResult\n$size$oculusRes" "Crack it|C_$md5sum" "Done|GameDone_$md5sum" "Blacklist it|BL_$md5sum" "" "Google|https://www.google.com/search?q=$google VR Quest" "Applab|https://applabgamelist.com/Best/$google"
		echo "Found NEW in FFA: $line." >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	else
		ourVersion=$(echo $exist | cut -d ';' -f4)
		if [ "$version" -gt "$ourVersion" ]; then
			echo "Found NEW version in FFA: $line. Autocracking on" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
			send_normal_message "-1001739439303" "Found new version in FFA $line. Auto cracking in progres...."
			echo "new version in FFA: $line" > "/home/ffa/web/logs/download$logFile.txt"
			rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "FFA-DD:/_donations/$line" /home/ffa/drive/autoCracked/_next
			savedid="${BOTSENT[ID]}"
			checkDownload "$line" "FFA" "$savedid" "$md5result" "$md5sum"
		fi
		echo "$md5sum;$line;FFA" >> /home/ffa/gameStatus.txt
	fi
done <<< "$ffa"
echo "VRP"
while IFS= read -r line;
do
	logFile=$(date +'%Y-%m-%d-%R:%S')
	if [ -z "$line" ]; then
			continue
	fi
	game=$(echo "$line" | sed 's/.zip//g' | rev | cut -d ' ' -f1 | rev)
	version=$(echo "$line" | rev | cut -d ' ' -f2 | rev | sed 's/v//g')
	google=$(echo "$line" | rev | cut -d ' ' -f3- | rev )
	exist=$(grep -i -F "$game" /home/ffa/drive/Quest\ Games/FFA2.txt)
	md5sum=$(echo -n "$line""VRP" | md5sum | awk '{printf $1}')
	md5sumCheck=$(rclone cat "FFA-DD:/md5sum/$line.md5sum" | cut -c -32)
	md5result=""
	if [ -z "$md5sumCheck" ]; then
		md5result="md5sum file not found!!"
	else
		md5sumFile=$(rclone md5sum "FFA-DD:/_donations/$line" | cut -c -32)
		if [ "$md5sumCheck" == "$md5sumFile" ]; then
			md5result="md5sum is correct"
		else
			md5result="md5sum failed !!!!!!!!"
		fi
	fi
	if [ -z "$exist" ]; then
		size=$(rclone size "RSL-gameuploads:/$line")
		echo "$md5result\nFound NEW in VRP: $line" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
		storeResult=$(searchPlayStore "$game")
		oculusDBresult=$(curl https://oculusdb.rui2015.me/api/v1/packagename/$game)
		price=$(echo $oculusDBresult | jq .baseline_offer.price.offset_amount)
		idoculus=$(echo $oculusDBresult | jq .id )
		idoculusFinal=${idoculus//[\"]/}
		website=$(echo $oculusDBresult | jq .website_url)
		oculusRes=""
		#if [ -n "$idoculus" ]; then;
			oculusRes="\nStore: oculus.com/experiences/quest/$idoculusFinal\nPrice in store is $price\nWebsite is $website"
		#fi
		send_inline_buttons "-1001739439303" "$md5result\nNEW in VRP: $line.$(searchPlayStore $game)\n$size$oculusDBresult" "Crack it|C_$md5sum" "Done|GameDone_$md5sum" "Blacklist it|BL_$md5sum" "" "Google|https://www.google.com/search?q=$google VR Quest" "Applab|https://applabgamelist.com/Best/$google"
		echo "$md5sum;$line;VRP" >> /home/ffa/gameStatus.txt
	else
		
		ourVersion=$(echo $exist | cut -d ';' -f4)
		if [ "$version" -gt "$ourVersion" ]; then
			echo "$md5sum;$line;VRP" >> /home/ffa/gameStatus.txt
			echo "Found NEW version in VRP: $line. Autocracking starting" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
			send_normal_message "-1001739439303" "Found new version in VRP $line. Auto cracking in progres...."
			echo "new version in VRP $line"  > "/home/ffa/web/logs/download$logFile.txt"
			rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "RSL-gameuploads:/$line" /home/ffa/drive/autoCracked/_next
			if [ -f "/home/ffa/drive/autoCracked/_next/$line" ] && [ -s "/home/ffa/drive/autoCracked/_next/$1" ] ; then
				rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "VRP_DONATION_TOKEN_1:/$line" /home/ffa/drive/autoCracked/_next
			fi
			if [ -f "/home/ffa/drive/autoCracked/_next/$line" ] && [ -s "/home/ffa/drive/autoCracked/_next/$1" ] ; then
				rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "VRP_DONATION_TOKEN_2:/$line" /home/ffa/drive/autoCracked/_next
			fi
			if [ -f "/home/ffa/drive/autoCracked/_next/$line" ] && [ -s "/home/ffa/drive/autoCracked/_next/$1" ] ; then
				rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "RSL-gameuploads2:/$line" /home/ffa/drive/autoCracked/_next
			fi
			savedid="${BOTSENT[ID]}"
			checkDownload "$line" "VRP" "$savedid" "$md5result" "$md5sum"
		fi
	fi
done <<< "$vrp"
echo "QU"
rm -rf /home/ffa/drive/autoCracked/_next/*
while IFS= read -r line;
do
	logFile=$(date +'%Y-%m-%d-%R:%S')
	if [ -z "$line" ]; then
		continue
	fi
	echo "Found NEW in Garr-Glohnma:/clean: $line" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	echo "Found NEW in Garr-Glohnma:/clean: $line" >> "/home/ffa/web/logs/download$logFile.txt"
	#send_inline_buttons "-1001739439303" "NEW in QU: $line." "Crack it|C_$md5sum" "Done|GameDone_$md5sum"
	rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "Garr-Glohnma:/clean/$line" /home/ffa/drive/autoCracked/_next
	cd /home/ffa/drive/autoCracked/_next
	for f in *\ *; do mv "$f" "${f// /_}"; done 2>/dev/null
	ZIPS="$(ls *.zip 2>/dev/null)"
	for ZIP in $ZIPS
	do
		unzip -o "$ZIP"
		rm -rf "$ZIP"
	done
	#Unrar any rars in the Desktop\QC\Next directory.
	RARS="$(ls *.rar 2>/dev/null)"
	for RAR in $RARS
	do
		RAR2=$(echo "$RAR" | tr -cd "+-.[:alnum:] ")
		mv -f "$RAR" "$RAR2" 2>/dev/null
		unrar x -o+ "$RAR2" 2>/dev/null
		rm -rf "$RAR2" 2>/dev/null
	done
	for f in *\ *; do mv "$f" "${f// /_}"; done 2>/dev/null
	rm *.txt 2>/dev/null
	find . -type d -depth -empty -exec rmdir "{}" \; 2>/dev/null
	find . -empty -type -f -delete 2>/dev/null
	APKS=$(ls *.apk 2>/dev/null)
	for APK in $APKS
	do
		game=$(aapt2 dump badging $APK | grep package:\ name | awk -F"'" '/package: name=/{print $2}')
		version=$(aapt2 dump badging $APK | grep versionCode= | awk -F"'" '/versionCode=/{print $4}')
		exist=$(grep -i -F "$game" /home/ffa/drive/Quest\ Games/FFA2.txt)
		md5sum=$(echo -n "$line""QU" | md5sum | awk '{printf $1}')
		tail /home/ffa/gameStatus.txt
		echo "$md5sum;$game;QU" >> /home/ffa/gameStatus.txt
		ourVersion=$(echo $exist | cut -d ';' -f4)
		if [ -z "$exist" ] || [ "$version" -gt "$ourVersion" ]; then
			echo "Found NEW version in QU: $line. Autocracking starting" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
			send_normal_message "-1001739439303" "Found new version in QU $line. Auto cracking in progres...."
			echo "new version in QU $line"  >> "/home/ffa/web/logs/download$logFile.txt"
			sleep 2;
			savedid="${BOTSENT[ID]}"
			checkDownload "$line" "QU" "$savedid" "" "$md5sum"
		else
			send_normal_message "-1001739439303" "Found old version in QU $line. Not cracking"
		fi
	done
	rm -rf /home/ffa/drive/autoCracked/_next/*
done <<< "$qu"
echo "QU2"
while IFS= read -r line;
do
	logFile=$(date +'%Y-%m-%d-%R:%S')
	if [ -z "$line" ]; then
		continue
	fi
	echo "Found NEW in Garr-Glohnma:/applab/clean/: $line" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
	echo "Found NEW in Garr-Glohnma:/applab/clean/: $line" >> "/home/ffa/web/logs/download$logFile.txt"
	rclone copy --log-file "/home/ffa/web/logs/download$logFile.txt" -v --stats 5s --bwlimit 40M "Garr-Glohnma:/applab/clean/$line" /home/ffa/drive/autoCracked/_next 
	cd /home/ffa/drive/autoCracked/_next
	for f in *\ *; do mv "$f" "${f// /_}"; done 2>/dev/null
	ZIPS="$(ls *.zip 2>/dev/null)"
	for ZIP in $ZIPS
	do
		unzip -o "$ZIP"
		rm -rf "$ZIP"
	done
	#Unrar any rars in the Desktop\QC\Next directory.
	RARS="$(ls *.rar 2>/dev/null)"
	for RAR in $RARS
	do
		RAR2=$(echo "$RAR" | tr -cd "+-.[:alnum:] ")
		mv -f "$RAR" "$RAR2" 2>/dev/null
		unrar x -o+ "$RAR2" 2>/dev/null
		rm -rf "$RAR2" 2>/dev/null
	done
	for f in *\ *; do mv "$f" "${f// /_}"; done 2>/dev/null
	rm *.txt 2>/dev/null
	find . -type d -depth -empty -exec rmdir "{}" \; 2>/dev/null
	find . -empty -type -f -delete 2>/dev/null
	APKS=$(ls *.apk 2>/dev/null)
	for APK in $APKS
	do
		game=$(aapt2 dump badging $APK | grep package:\ name | awk -F"'" '/package: name=/{print $2}')
		version=$(aapt2 dump badging $APK | grep versionCode= | awk -F"'" '/versionCode=/{print $4}')
		exist=$(grep -i -F "$game" /home/ffa/drive/Quest\ Games/FFA2.txt)
		md5sum=$(echo -n "$line""QU" | md5sum | awk '{printf $1}')
		existsInFFA=$( grep -F -f /home/ffa/listedGames.txt)
		echo "$md5sum;$game;QU" >> /home/ffa/gameStatus.txt
		ourVersion=$(echo $exist | cut -d ';' -f4)
		if [ -z "$exist" ] || [ "$version" -gt "$ourVersion" ] ; then
			echo "Found NEW version in QU: $line. Autocracking starting" >> "/home/ffa/web/logs/MasterLog$(date +'%Y-%m-%d').txt"
			send_normal_message "-1001739439303" "Found new version in QU $line. Auto cracking in progres...."
			echo "new version in QU $line" >> "/home/ffa/web/logs/download$logFile.txt"
			sleep 2;
			savedid="${BOTSENT[ID]}"
			checkDownload "$line" "QU" "$savedid" "" "$md5sum"
		else
			send_normal_message "-1001739439303" "Found old version in QU $line. Not cracking"
		fi
	done
	rm -rf /home/ffa/drive/autoCracked/_next/*
done <<< "$qu2"
sort -u /home/ffa/gameStatus.txt > /home/ffa/gameStatusTEMP.txt
rm /home/ffa/gameStatus.txt
mv /home/ffa/gameStatusTEMP.txt /home/ffa/gameStatus.txt
rm -f ~/detectNewGames.lock