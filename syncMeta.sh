#!/bin/bash
source /home/ffa/drive/scriptFiles/globalSciptConfig.sh

if { set -C; 2>/dev/null >~/manlock.lock; }; then
        trap "rm -f ~/manlock.lock" EXIT
else
        echo "Lock file exists… exiting"
        echo "Lock file exists… exiting" >> /var/log/ffa/sync.log
        exit
fi
sort -u -f /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt -o /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt
echo "--------------------------------------------" >> /var/log/ffa/sync.log
echo "Starting new sync: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/sync.log
echo "Generating game list" >> /var/log/ffa/sync.log
start=`date +%s.%N`
/home/ffa/ffascripts/createGameList.sh >| /var/log/ffa/gameList.log 2>&1
cp /home/ffa/drive/Quest\ Games/FFA.txt  "/home/ffa/drive/scriptFiles/backup/FFA$(date +%d-%m-%Y).txt"
cp /home/ffa/drive/Quest\ Games/.meta/nouns/blacklist.txt  "/home/ffa/drive/scriptFiles/backup/blacklist$(date +%d-%m-%Y).txt"
end=`date +%s.%N`
echo "list done. Used time $( echo "$end - $start" | bc -l )" >> /var/log/ffa/sync.log
echo "Starting Quest sync: $(date +'%R:%S-%d/%m/%Y')" >> /var/log/ffa/sync.log
for mirror in ${questMirrors[@]}
do
        echo -n "FFA-$mirror started at $(date +'%R:%S-%d/%m/%Y')..."  >> /var/log/ffa/sync.log
        rclone sync --drive-chunk-size 128M --log-file /var/log/ffa/syncProgress.log -v --stats 60s --transfers 15 --checkers 8 --contimeout 60s --timeout 300s --retries 3 --low-level-retries 10 /home/ffa/drive/Quest\ Games/.meta FFA-"$mirror":/Quest\ Games/.meta
        echo " done"  >> /var/log/ffa/sync.log
done
