#!/bin/bash
source /home/ffa/drive/scriptFiles/globalSciptConfig.sh
if [ "$stopCreateGameList" = true ] ; then
    echo 'GameList creation is stopped!! exiting...'
	exit
fi
echo "Generaing new game list..."
echo "Game Name;Release Name;Package Name;Version Code;Last Updated;Size (MB)" > /home/ffa/drive/Quest\ Games/FFA.txt
find "/home/ffa/drive/Quest Games" -name "* *" -type d | rename 's/  */ /g'
#ls -l "/home/ffa/drive/Quest Games" > temp.txt
find "/home/ffa/drive/Quest Games" -maxdepth 1 -type d -printf '%P\n' > temp.txt

failed=false
while IFS= read -r line; do
	if [[ "$line" =~ ".txt" ]]; then
			continue;
	fi
	if [[ "$line" =~ ".meta" ]]; then
			continue;
	fi
	[ -z "$line" ] && continue
	releaseName=$(echo "$line")
	exception=$(cat /home/ffa/drive/scriptFiles/exceptions.txt | grep "$releaseName")
	if [ -n "${exception}" ]; then
		echo "FOUND \'$exception\' in exceptions for '$releaseName'"
		size=$(du -ms "/home/ffa/drive/Quest Games/$releaseName" | sed 's/[[:space:]]/ /g' | cut -d ' ' -f 1)
		it=$(date -r "$apk" +"%Y-%m-%d %H:%M UTC")
		echo "$exception;$lastUpdate;$size" >> /home/ffa/drive/Quest\ Games/FFA.txt
	else
		path=$(echo "$releaseName" | sed 's/ /\\ /g' )
		apk=$(ls "/home/ffa/drive/Quest Games/$releaseName/"*apk)
		packageName=$(aapt dump badging "$apk" | grep package:\ name | awk -F"'" '/package: name=/{print $2}')
		if [ -z "${packageName}" ]; then
			echo "Package empty for $releaseName, will try alternative aapt"
			packageName=$(aapt2 dump badging "$apk" | grep package:\ name | awk -F"'" '/package: name=/{print $2}')
		fi
		if [ -z "${packageName}" ]; then
			echo "Package empty, not adding line"
			failed=true
		else
			label=$(aapt dump badging "$apk" | grep application-label | awk -F"'" '/application-label:/{print $2}')
			gameName=$(echo $releaseName | awk -F 'v[0-9]' ' { print $1 }')
			versionCode=$(aapt dump badging "$apk" | grep versionCode= | awk -F"'" '/versionCode=/{print $4}')
			aplicationLabel=$(aapt dump badging "$apk" | grep application-label | awk -F"'" '/application-label:/{print $2}')
			size=$(du -ms "/home/ffa/drive/Quest Games/$releaseName" | sed 's/[[:space:]]/ /g' | cut -d ' ' -f 1)
			lastUpdate=$(date -r "$apk" +"%Y-%m-%d %H:%M UTC")
			echo "$gameName;$releaseName;$packageName;$versionCode;$lastUpdate;$size" >> /home/ffa/drive/Quest\ Games/FFA.txt
			#echo "$label;$releaseName;$packageName;$versionCode;$lastUpdate;$size"
		fi
	fi
done < temp.txt
rm  /home/ffa/drive/Quest\ Games/FFA2.txt /home/ffa/drive/Quest\ Games/FFA3.txt /home/ffa/drive/Quest\ Games/FFA4.txt
cp /home/ffa/drive/Quest\ Games/FFA.txt /home/ffa/drive/Quest\ Games/FFA2.txt
cp /home/ffa/drive/Quest\ Games/FFA.txt /home/ffa/drive/Quest\ Games/FFA3.txt
cp /home/ffa/drive/Quest\ Games/FFA.txt /home/ffa/drive/Quest\ Games/FFA4.txt
rm temp.txt
echo "game list done"
if [ "$failed" == true ]; then
	echo "Errors detected... sending message to group"
#	/home/ffa/telegram-bot-bash/bin/send_message.sh "-1001763061332" "It seems like there are errors one the game list generation, please fix it or I will remind you every time!!\nSync still in progress. Watch logs here $(cat /var/log/ffa/gameList.log | pastebinit)" &
fi
